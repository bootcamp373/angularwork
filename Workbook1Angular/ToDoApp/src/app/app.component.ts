import { Component } from '@angular/core';
import { ToDosComponent } from './to-dos/to-dos.component';
import { HeaderComponent } from './header/header.component';
// import { ToDo } from './to-do.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'ToDoApp';
}
// import { Component, OnInit } from '@angular/core';
// import { EmployeeService } from './providers/employee.service';
// import { Employee } from './models/employee.model';
// @Component({
// selector: 'app-root,
// templateUrl: './app.component.html',
// styleUrls: ['./app.component.css']
// })
// export class AppComponent implements OnInit {
// employees: Array<Employee> = [];
// constructor(private employeeService: EmployeeService) {}
// ngOnInit() {
// // call getEmployees() method in EmployeeService
// this.employeeService.getEmployees().subscribe(data => {
// this.employees = data.employees;
// });
// }
// }