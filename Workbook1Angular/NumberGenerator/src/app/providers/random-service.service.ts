import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class RandomServiceService {

  constructor() { }

  public getRandomNumber(): number {
    return this.getRandomInt(1,100);
  }

  private getRandomInt(min: number, max: number) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min) + min); // The maximum is exclusive and the minimum is inclusive
  }
}


