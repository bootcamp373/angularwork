import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DisplayRandomComponent } from './display-random/display-random.component';
import { RandomServiceService } from './providers/random-service.service';

@NgModule({
  declarations: [
    AppComponent,
    DisplayRandomComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [RandomServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
