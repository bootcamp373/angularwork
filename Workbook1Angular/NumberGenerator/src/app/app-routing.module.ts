import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DisplayRandomComponent } from './display-random/display-random.component';

const routes: Routes = [
  {
    path: "", component: DisplayRandomComponent
  },
  {
    path: "display-random", component: DisplayRandomComponent
  },

];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
